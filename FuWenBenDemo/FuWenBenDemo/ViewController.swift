//
//  ViewController.swift
//  FuWenBenDemo
//
//  Created by 邱学伟 on 2016/11/18.
//  Copyright © 2016年 邱学伟. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        FuWenBenDemo()
    }
    
    //需求 邱学伟是大帅哥(加个笑脸图片)   邱学伟:红色背景绿色字体加粗显示   是:蓝色字体 10号小字体  大帅哥:灰色42号字体
    fileprivate func FuWenBenDemo() {
        
        //定义富文本即有格式的字符串
        let attributedStrM : NSMutableAttributedString = NSMutableAttributedString()
        
        //邱学伟
        let qiuxuewei : NSAttributedString = NSAttributedString(string: "邱学伟", attributes: [ NSBackgroundColorAttributeName : UIColor.red,NSForegroundColorAttributeName : UIColor.green, NSFontAttributeName : UIFont.boldSystemFont(ofSize: 28.0)]) //(string: "邱学伟")
        //是
        let shi : NSAttributedString = NSAttributedString(string: "是", attributes: [NSForegroundColorAttributeName : UIColor.blue, NSFontAttributeName : UIFont.systemFont(ofSize: 10.0)])
        //大帅哥
        let dashuaige : NSAttributedString = NSAttributedString(string: "大帅哥", attributes: [NSForegroundColorAttributeName : UIColor.lightGray, NSFontAttributeName : UIFont.systemFont(ofSize: 42.0)])
        //笑脸图片
        let smileImage : UIImage = UIImage(named: "d_hehe")!
        let textAttachment : NSTextAttachment = NSTextAttachment()
        textAttachment.image = smileImage
        textAttachment.bounds = CGRect(x: 0, y: -4, width: 22, height: 22)
        
        attributedStrM.append(qiuxuewei)
        attributedStrM.append(shi)
        attributedStrM.append(dashuaige)
        attributedStrM.append(NSAttributedString(attachment: textAttachment))
        
        label.attributedText = attributedStrM
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

